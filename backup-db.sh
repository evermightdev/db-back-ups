#!/bin/bash

path_archive=/root/scheduled-backups/archive
path_processing=/root/scheduled-backups/processing
date_stamp=`date +\%y\%m\%d\%H\%M`

mkdir -p $path_archive
mkdir -p $path_processing
mysqldump --login-path=rootlocal donationfarm_prod > "$path_processing/donationfarm_prod-$date_stamp.sql"
mysqldump --login-path=rootlocal startmission > "$path_processing/sm-$date_stamp.sql"
mysqldump --login-path=rootlocal donationfarm_wiki > "$path_processing/donationfarm_wiki-$date_stamp.sql"
mysqldump --login-path=rootlocal business_wiki > "$path_processing/biz_wiki-$date_stamp.sql"

tar rvf "$path_processing/backup-$date_stamp.tar" "$path_processing/"*.sql
rm "$path_processing/"*.sql
gzip "$path_processing/"*.tar
mv "$path_processing/"*.tar.gz "$path_archive/"
find "$path_archive/" -mtime +90 -type f -delete