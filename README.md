Create a user in mysql

```
GRANT USAGE ON *.* TO 'readonly'@'%' IDENTIFIED BY 'readonly';
GRANT SELECT, LOCK TABLES ON `rmhc1`.* TO 'readonly'@'localhost';
GRANT SELECT, LOCK TABLES ON `twill1`.* TO 'readonly'@'localhost';
```

Create a login path for that user

```
mysql_config_editor set --login-path=readuser --host=localhost --user=readonly --password;
```

You will get a .mylogin.cnf in your accounts home directory.
