#!/bin/bash

path_archive=/root/scheduled-backups/archive
path_processing=/root/scheduled-backups/processing
date_stamp=`date +\%y\%m\%d\%H\%M`

mkdir -p $path_archive
mkdir -p $path_processing
mysqldump -u wordpress -p'somepass' nutriag > "$path_processing/db.sql";
tar cvf "$path_processing/backup-$date_stamp.tar" \
  --exclude=/var/www/wordpress/wp-content/ai1wm-backups \
  --exclude=/var/www/wordpress/wp-content/uploads \
  --exclude=/var/www/wordpress/*/wp-content/uploads \
  --exclude=/var/www/wordpress/staging-* \
  --exclude=/var/www/wordpress/wp-content/updraft \
  --exclude=/var/www/wordpress/temp \
  /var/www/wordpress
tar rvf "$path_processing/backup-$date_stamp.tar" "$path_processing/db.sql"
rm "$path_processing/db.sql"
gzip "$path_processing/"*.tar
mv "$path_processing/"* "$path_archive/"
find "$path_archive/" -mtime +25 -type f -delete